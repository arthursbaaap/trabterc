/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabterctrim;

/**
 *
 * @author Aluno
 */
public class Cliente {
    private String nome;
    private int cpf;
    private String email;
    private int tel_contato;
    
    public String getNome(){
    return nome;
    }
    
    public void setNome(String nome){
    this.nome=nome;
    }
    
    public int getCpf(){
    return cpf;
    }
    
    public void setCpf(int cpf){
    this.cpf=cpf;
    }
    
     public String getEmail(){
    return email;
    }
    
    public void setEmail(String email){
    this.email=email;
    }
    
    public int getTel_contato(){
    return tel_contato;
    }
    
    public void setTel_contato(int tel_contato){
    this.tel_contato=tel_contato;
    }
}
