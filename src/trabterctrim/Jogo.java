/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabterctrim;

/**
 *
 * @author Aluno
 */
public class Jogo {
    private int cod_jogo;
    private String genero;
    private String nome_jogo;
    private int preco;

    public int getCod_jogo() {
        return cod_jogo;
    }

    public void setCod_jogo(int cod_jogo) {
        this.cod_jogo = cod_jogo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getNome_jogo() {
        return nome_jogo;
    }

    public void setNome_jogo(String nome_jogo) {
        this.nome_jogo = nome_jogo;
    }

    public int getPreco() {
        return preco;
    }

    public void setPreco(int preco) {
        this.preco = preco;
    }
    
    
    
}
